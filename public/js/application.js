// convert board variable to display on view
var setBoard = function(board) {
  for(var y = 0; y < board.length; y++) {
    for(var x = 0; x < board[y].length; x++) {
      if(board[y][x] != $("#position" + y + "-" + x).hasClass("active")) {
        switch (board[y][x]) {
        case true:
          $("#position" + y + "-" + x).addClass("active");
          break;
        case false:
          $("#position" + y + "-" + x).removeClass("active");
          break;
        }
      }
    }
  }
};

// basic logic to determine cell is alive or dead
var nextStep = function(board) {
  // have to declare a new board so that the changed value will not affect the following calculations
  var spareBoard = [];
  for(var y = 0; y < 60; y++) {
    spareBoard.push([]);
    for(var x = 0; x < 85; x++) {
      spareBoard[y].push(board[y][x]);
    }
  }

  var row = "";
  for(var y = 0; y < board.length; y++) {
    for(var x = 0; x < board[y].length; x++) {
      // starting logic to calculate the numbers of alive neighbor cell
      var neighbor = 0;
      if(y > 0 && x > 0 && board[y - 1][x - 1])
        neighbor++;
      if(y > 0 && board[y - 1][x])
        neighbor++;
      if(y > 0 && x < board[y].length - 1 && board[y - 1][x + 1])
        neighbor++;
      if(x > 0 && board[y][x - 1])
        neighbor++;
      if(x < board[y].length - 1 && board[y][x + 1])
        neighbor++;
      if(y < board.length - 1 && x > 0 && board[y + 1][x - 1])
        neighbor++;
      if(y < board.length - 1 && board[y + 1][x])
        neighbor++;
      if(y < board.length - 1 && x < board[y].length - 1 && board[y + 1][x + 1])
        neighbor++;
      row = row.concat(neighbor + " ");

      // revive cell if there is exactly 3 live neighbor
      if(neighbor == 3 && !board[y][x]) {
        spareBoard[y][x] = true;
        $("#position" + y + "-" + x).addClass("active");
      }
      // kill cell if there is less than 2 or more than 3 neighbor
      if((neighbor < 2 || neighbor > 3) && board[y][x]) {
        spareBoard[y][x] = false;
        $("#position" + y + "-" + x).removeClass("active");
      }
    }
    console.log(row);
    row = "";
  }
  return spareBoard;
};

var createGameCell = function(board) {
  // create a div with designated ID and class for every cell
  for(var y = 0; y < board.length; y++) {
    var newRow = $("<div id='position" + y + "' class='gamerow'></div>");
    $("#gameboard").append(newRow);
    for(var x = 0; x < board[y].length; x++) {
      var newCell = $("<div id='position" + y + "-" + x + "' class='gamecell'></div>")
      newRow.append(newCell);
    }
  }
};

$(document).ready(function() {
  var board = [];
  var auto = null;
  for(var y = 0; y < 60; y++) {
    board.push([]);
    for(var x = 0; x < 85; x++) {
      board[y].push(false);
    }
  }

  createGameCell(board);

  // set the next frame
  $("#nextstep").click(function() {
    board = nextStep(board);
  });

  // set a timer or stop a timer when start button is clicked
  $("#autostep").click(function() {
    switch(auto) {
    case null:
      auto = setInterval(function() {
        board = nextStep(board);
      }, 500);
      $("#autostep").html("Stop");
      break;
    default:
      clearInterval(auto);
      auto = null;
      $("#autostep").html("Start");
      break;
    }
  });

  // a common function to revive or kill a cell depending on whether its dead or alive when clicked
  $(".gamecell").click(function() {
    // find the position of the cell
    var cellID = this.id;
    var y = cellID.substring(cellID.indexOf('n') + 1, cellID.indexOf('-'));
    var x = cellID.substring(cellID.indexOf('-') + 1);
    switch(board[y][x]) {
    case true:
      // kill cell if its alive
      board[y][x] = false;
      $("#position" + y + "-" + x).focus();
      $("#position" + y + "-" + x).removeClass("active");
      break;
    case false:
      // revive cell if its dead
      board[y][x] = true;
      $("#position" + y + "-" + x).focus();
      $("#position" + y + "-" + x).addClass("active");
      break;
    }
  });

  $("#clearboard").click(function() {
    for(var y = 0; y < 60; y++) {
      for(var x = 0; x < 85; x++) {
        board[y][x] = false;
      }
    }
    setBoard(board);
  });

  $("#glider").click(function() {
    for(var y = 0; y < 60; y++) {
      for(var x = 0; x < 85; x++) {
        switch(y + "|" + x) {
        case "30|41":
        case "28|42":
        case "30|42":
        case "29|43":
        case "30|43":
          board[y][x] = true;
          break;
        default:
          board[y][x] = false;
          break;
        }
      }
    }
    setBoard(board);
  });

  $("#exploder").click(function() {
    for(var y = 0; y < 60; y++) {
      for(var x = 0; x < 85; x++) {
        switch(y + "|" + x) {
        case "27|40":
        case "28|40":
        case "29|40":
        case "30|40":
        case "31|40":
        case "27|42":
        case "31|42":
        case "27|44":
        case "28|44":
        case "29|44":
        case "30|44":
        case "31|44":
          board[y][x] = true;
          break;
        default:
          board[y][x] = false;
          break;
        }
      }
    }
    setBoard(board);
  });

  $("#spaceship").click(function() {
    for(var y = 0; y < 60; y++) {
      for(var x = 0; x < 85; x++) {
        switch(y + "|" + x) {
        case "29|40":
        case "31|40":
        case "28|41":
        case "28|42":
        case "28|43":
        case "31|43":
        case "28|44":
        case "29|44":
        case "30|44":
          board[y][x] = true;
          break;
        default:
          board[y][x] = false;
          break;
        }
      }
    }
    setBoard(board);
  });

  $("#tumbler").click(function() {
    for(var y = 0; y < 60; y++) {
      for(var x = 0; x < 85; x++) {
        switch(y + "|" + x) {
        case "30|39":
        case "31|39":
        case "32|39":
        case "27|40":
        case "28|40":
        case "32|40":
        case "27|41":
        case "28|41":
        case "29|41":
        case "30|41":
        case "31|41":
        case "27|43":
        case "28|43":
        case "29|43":
        case "30|43":
        case "31|43":
        case "27|44":
        case "28|44":
        case "32|44":
        case "30|45":
        case "31|45":
        case "32|45":
          board[y][x] = true;
          break;
        default:
          board[y][x] = false;
          break;
        }
      }
    }
    setBoard(board);
  });

  $("#glidergun").click(function() {
    for(var y = 0; y < 60; y++) {
      for(var x = 0; x < 85; x++) {
        switch(y + "|" + x) {
        case "25|24":
        case "26|24":
        case "25|25":
        case "26|25":
        case "26|32":
        case "27|32":
        case "25|33":
        case "27|33":
        case "25|34":
        case "26|34":
        case "27|40":
        case "28|40":
        case "29|40":
        case "27|41":
        case "28|42":
        case "24|46":
        case "25|46":
        case "23|47":
        case "25|47":
        case "23|48":
        case "24|48":
        case "35|48":
        case "36|48":
        case "35|49":
        case "37|49":
        case "35|50":
        case "23|58":
        case "24|58":
        case "23|59":
        case "24|59":
        case "30|59":
        case "31|59":
        case "32|59":
        case "30|60":
        case "31|61":
          board[y][x] = true;
          break;
        default:
          board[y][x] = false;
          break;
        }
      }
    }
    setBoard(board);
  });
});
