# Game of Life #

### What is this repository for? ###

* A simple game to mock up John Conway's game of life
* Version 1.0

### How do I get set up? ###

* Just download the files
* Open index.html in any browser to get started

### Who do I talk to? ###

* Ching Yaw Hao
* I can be contacted through chingyawhao14@gmail.com